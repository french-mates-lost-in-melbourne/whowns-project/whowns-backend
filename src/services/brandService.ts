import Brand from "../models/nodes/brand";
import GenericService from "./genericService";
import {Neo4jCommandFactory, ReturnResult} from "neo4j-func";
// import ReturnResult from "../utils/returnResult";

export default class BrandService extends GenericService<Brand> {

    private static generic: GenericService<Brand> = new GenericService();

    static findAll(): Promise<Brand[]> {
        return Neo4jCommandFactory.findAll(new Brand())
            .then(this.generic.convertArray);
    }

    static findById(id: number): Promise<Brand> {
        return Neo4jCommandFactory.findById(new Brand(), id)
            .then(this.generic.convert);
    }
    static findByName(name: string): Promise<Brand[]> {
        return Neo4jCommandFactory.findByProp(new Brand(), 'name', name)
            .then(this.generic.convertArray);
    }

    static findByAnyName(name: string): Promise<Brand[]> {
        if (!name) {
            return this.findAll();
        }
        return Neo4jCommandFactory.findByAnyProp(new Brand(), 'name', name)
            .then(this.generic.convertArray);
    }

    static create(brand: Brand): Promise<ReturnResult> {
        return Neo4jCommandFactory.create(brand);
    }
}