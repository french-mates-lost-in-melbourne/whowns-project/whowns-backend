import { Neo4jCommandFactory, Neo4jCommand, Linking, ReturnResult, NodeNeo4J, anyEntity as any}  from "neo4j-func";
import Worker from "../models/nodes/worker";
import Brand from "../models/nodes/brand";
import Manage from "../models/links/manage";
import GenericService from "./genericService";

export default class WorkerService {
    private static generic: GenericService<Worker> = new GenericService();

    static findAll(): Promise<Worker[]> {
        return Neo4jCommandFactory.findAll(new Worker())
        .then(this.generic.convertArray);
    }

    static findAllWithManage(): Promise<ReturnResult> {
        const w = new Worker();
        const b = new Brand()
        return new Neo4jCommand()
            .match(new Linking(w).link(any()).to(b))
            .returnValue(new Linking(w).link(any()).to(b))
            .returnValue(b)
            .returnValue(w.lastName)
            .run();
    }

    static findWithManage(worker: Worker): Promise<ReturnResult> {
        // MATCH (w:Woker)
        // WHERE worker.lastName = $lastName
        // RETURN (w)-[]->(:Brand)
        return new Neo4jCommand()
            .match(worker)
            .where(worker.lastName).equals(worker.value(worker.lastName))
            .returnValue(new Linking(worker).link(any()).to(new Brand()))
            .run();
    }

    static findByLastname(lastName: string): Promise<Worker[]> {
        return Neo4jCommandFactory.findByProp(new Worker(), 'lastName', lastName)
        .then(this.generic.convertArray);
    }

    static create(worker: Worker): Promise<ReturnResult> {
        return new Neo4jCommand().create(worker).run();
    }

    static addManage(worker: Worker, brand: Brand, startDate: Date): Promise<ReturnResult> {
        return new Neo4jCommand()
            .match(worker)
            .where(worker.lastName).equals(worker.properties.get(worker.lastName.property))
            .match(brand)
            .where(brand.name).equals(brand.properties.get(brand.name.property))
            .create(new Linking(worker).link(new Manage(startDate)).to(brand))
            .run();
    }


    private static workerify(nodes: NodeNeo4J[]): Worker[] {
        return nodes as Worker[];
    }
}