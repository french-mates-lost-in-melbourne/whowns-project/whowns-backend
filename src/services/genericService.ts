import { NodeNeo4J } from "neo4j-func";

export default class GenericService<T> {

    convert(a: NodeNeo4J): T {
        return a as unknown as T;
    }
    convertArray(a: NodeNeo4J[]): T[] {
        return a as unknown as T[];
    }
}