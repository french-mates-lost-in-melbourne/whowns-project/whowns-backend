import express from 'express';
import WorkerService from '../services/workerService';
import Worker from '../models/nodes/worker';
import Brand from '../models/nodes/brand';
const router = express.Router();

router.get('/', (req: any, res: any, next: any) => {
  WorkerService.findAll()
    .then(value => {
      res.status(200).send(value);
    }).catch(err => {
      res.status(422).send(err);
    });
});

router.post('/', (req: any, res: any, next: any) => {
  const w = new Worker('Emilien', 'Nicolas');
  WorkerService.create(w)
    .then(value => {
      res.status(200).send(value.summary);
    }).catch(err => {
      res.status(422).send(err);
    });
});


router.get('/manage', (req: any, res: any, next: any) => {
  WorkerService.findAllWithManage()
    .then(value => {
      res.status(200).send(value.records);
    }).catch(err => {
      res.status(422).send(err);
    });
});

router.post('/manage', (req: any, res: any, next: any) => {
  const worker = new Worker('Emilien', 'Nicolas');
  const brand = new Brand('Nike');
  WorkerService.addManage(worker, brand, new Date())
    .then(value => {
      res.status(200).send(value.summary)
    }).catch(err => {
      res.status(422).send(err);
    });
});

export default router;
