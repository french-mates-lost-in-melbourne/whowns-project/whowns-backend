import express from 'express';
import Brand from '../models/nodes/brand';
import BrandService from '../services/brandService';
const router = express.Router();

router.get('/', (req: any, res: any, next: any) => {
  BrandService.findByAnyName(req.query.q)
    .then(value => {
      res.status(200).send(value);
    }).catch(err => {
      res.status(422).send(err);
    });
});

router.get('/:id', (req: any, res: any, next: any) => {
  const id = parseInt(req.params.id)
  if(isNaN(id)) {
    res.status(422).send({
      err: 'wrong id'
    });
  }
  BrandService.findById(id)
    .then(value => {
      res.status(200).send(value);
    }).catch(err => {
      res.status(422).send(err);
    });
});

router.post('/', (req: any, res: any, next: any) => {
  const {name, description, logo} = req.body;
  const brand: Brand = new Brand(name, description, logo);
  BrandService.create(brand)
    .then(value => {
      res.status(200).send(value);
    }).catch(err => {
      res.status(422).send(err);
    });
});

export default router
module.exports = router;
