import express from 'express';
const router = express.Router();

/* GET home page. */
router.get('/', (req: any, res: any, next: any) => {
  res.status(200).send({ api: 'whoms', info: 'respond' });
});

export default router;
module.exports = router;
