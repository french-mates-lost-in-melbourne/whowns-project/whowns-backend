import { PropertyDefinition, property, notNull, NodeNeo4J, node } from "neo4j-func";

@node('Worker')
export default class Worker extends NodeNeo4J {

    @property()
    @notNull
    firstName: PropertyDefinition<string>;
    @property()
    @notNull
    lastName: PropertyDefinition<string>;

    constructor(firstName?: string, lastName?: string) {
        super();
    }
}