import { PropertyDefinition, property, notNull, NodeNeo4J, node } from "neo4j-func";

@node('Brand')
export default class Brand extends NodeNeo4J {

    @property()
    @notNull
    name: PropertyDefinition<string>;
    @property()
    @notNull
    description: PropertyDefinition<string>;
    @property()
    logo: PropertyDefinition<string>;

    constructor(name?: string, description?: string, logo?: string) {
        super();
    }
}