import { PropertyDefinition, property, notNull, LinkNeo4J, link } from "neo4j-func";

@link('Manages')
export default class Manage extends LinkNeo4J {

    @property()
    @notNull
    startDate: PropertyDefinition<Date>;
    @property()
    endDate: PropertyDefinition<Date>;

    constructor(startDate?: Date, endDate?: Date) {
        super();
    }
}